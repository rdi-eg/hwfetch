# hwfetch
hwfetch is a program that its goal is to turn
`.gitlab-ci.yml` into single line scripts.
- arguments (optinal):
    - -e
        * enables encryption/decryption mode in building the root repo andf its dependencies. 

The way it achieves that is by building a graph of dependencies
where each node is an RDI C++ project.
Going through that graph depth first we recursively analyze the
dependencies and satisfy them all through 3 simple steps:
- clone
- build
- install

Direct dependencies are stored in the `requires.txt` which should contain the **http(s)** urls for the git repos. One per line.

A direct dependency is a library that you include headers from directly in your project.

### Clone
dependencies are cloned in `/tmp/hwfetch`

### Build
* RDI currently uses the qmake tool build chain and so the build step is implemented accordingly.
* Some projects only contain headers and do not have .pro file in them. They are gracefully handled.
* Some projects have `lib/`, `test/` or `app/` sub-directories. Some projects don't have any sub-directories. All cases are handled gracefully.
* The `TARGET` in the test sub-project's .pro must be named `test`.
* builds the test project only of the root project and does not build the tests of the dependencies.

### Install
* hwfetch assumes a CI environment; which is usually a docker image.
* installs headers and static libraries into `/usr/include/` and `/usr/lib/` respectively.
* installs these headers and static libs into `artifacts/include/` and `artifacts/lib/` respectively to be used in the testing stages in CI.
* installs the binaries (from `test/` or `app/`) into `artifacts/bin/`


### Swig build 
* exceutes if a `java_test/` or `python_test/` exists in root repo path
* arguments : language (python or java)
* run configue file in `lib/`
* copy all interface files and the dynamic library to `artifacts/deploy/language/`
* The .so file is named: `lib + TARGET + .so` where `TARGET` is the target value in the `lib.pro`

### Java compile
* exceutes if a `java_test/` exists in root repo path
* compiles jar files in `java_test/src/`
* puts the output in `java_test/bin/`

# hwjava (Java test script)
* Runs the .class file which is located in `java_test/bin/`
* Copies the .so to `/usr/lib/`. Write your tests accordingly.
* Copies `artifacts/models` into `/opt/rdi/models/`

# hwtest  (C++ test script)
* hwtest will take the artifacts from the build stage in CI and will install them to the appropriate system directories, ie: `/usr/lib`, `/usr/include` and `/usr/bin`
* The testing binary is run. you can provide arguments to hwtest and will be passed to the test bin
* example hwtest "recognize test case"

# hwdeploy
- arguments
    - ftp path to the deployment directory
    - zip file name which will contain the deployment package
    - langugage (python, java)
    - encrypted/non-encrypted
* takes everything in `artifacts/deploy/language`
* zip those files
* and copy them to the destination specified in the arguments
* name the zip file as specified in the arguments
* (E.g.) `hwdeploy ninja@turtle.rdi:/home/ninja/cpp_team/GIZ/ContinusDelivery/ giz_engine python encrypted`

## Usage
In the .gitlab-ci.yml file:

```yml
stages:
  - build
  - test
  - deploy
    
build:
  stage: build
  tags:
    - docker
  artifacts:
    paths:
      - artifacts/
  script:
    - hwfetch
    
cpp_test:
  stage: test
  tags:
    - docker
  script:
    - hwtest "End to End"

java_test:
  stage: test
  tags:
    - docker
  script:
    - hwjava

deploy:
  stage: deploy
  tags:
    - shell
  script:
    - hwdeploy ninja@turtle.rdi:/home/ninja/cpp_team/GIZ/ContinusDelivery/ giz_engine java encrypted
   
```

That's it! :D it'll do all the magic on its own.
Have fun :*