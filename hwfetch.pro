TEMPLATE = app
CONFIG += console c++1z

QMAKE_CXXFLAGS += -fPIC -std=c++17 -fopenmp

CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        main.cpp

INCLUDEPATH += /opt/rdi/include

CONFIG(release, debug|release) {
    LIBS += -L/opt/rdi/lib
}
CONFIG(debug, debug|release) {
    LIBS += -L/opt/rdi/lib_debug
    DEFINES += __DEBUG__
}

LIBS += -lwfile -lstdc++fs -lrdi_subprocess -fopenmp
