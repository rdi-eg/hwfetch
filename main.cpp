#include <algorithm>
#include <cstdlib>
#include <iostream>
#include <map>
#include <string>
#include <unistd.h>
#include <vector>

#include <rdi_stl_utils.hpp>
#include <rdi_subprocess.hpp>
#include <rdi_wfile.hpp>

using namespace std;
using namespace RDI;

#define REQUIRES_FILENAME "requires.txt"
#define HWFETCH_VERSION 9
#define REPOS_PATH "/tmp/hwfetch/"

static bool rdi_encrypt = false;
static bool unit_counter = false;

struct Defer
{
	void (*defered)(void);

	Defer(void (*f)(void))
	{
		defered = f;
	}

	~Defer()
	{
		defered();
	}
};

#define DEFER(exp) Defer ___d___([]() { exp; })

struct DependencyNode
{
	vector<string> dependencies{};
	vector<string> subprojects{};
	string branch{};
	bool satisfied = false;
};

static map<string, DependencyNode> dependencies_map = {};

static
#ifndef __DEBUG__
	const
#endif
	string root_repo_path
	= get_current_directory();

template <typename... T>
string get_stdout_from_cmd(const char* c, T... args)
{
	int status;
	RDI_Buffer output_b = rdi_subprocess_o(&status, c, args..., nullptr);

	string output(output_b.data, output_b.size);

	rdi_buffer_free(output_b);

	return output;
}

string extract_projectname(const string& url)
{
	size_t slash_i = url.find_last_of('/') + 1;
	size_t dot_i   = url.find_last_of('.');

	return string(url.begin() + long(slash_i), url.begin() + long(dot_i));
}

void execute(const string& command)
{
	cout << command << endl;
	throw_unless(system(command.c_str()) == 0, runtime_error("Command returned non-zero"));
}

/// He protecc
/// He attacc
/// But most importantly
/// He interjecc
///
/// We interjecc internal projects and we insert the $CI_JOB_TOKEN
/// environment variable to the clone url. Which is necessary to have it
/// authenticated to gitlab.
///
/// To learn more:
/// https://docs.gitlab.com/ce/user/project/new_ci_build_permissions_model.html#dependent-repositories
string interjecc(string url)
{
	string beginning(url.begin(), url.begin() + 22);

	if (beginning != "http://arwash.rdi:5050")
	{
		return url;
	}

	string output = "http://gitlab-ci-token:${CI_JOB_TOKEN}@arwash.rdi:5050";
	output += string(url.begin() + 22, url.end());

	return output;
}

void clone(string url, const string& target_path, const string& branch = "")
{
	string command = "git clone ";

	if (branch.size())
	{
		command += "-b " + branch + " ";
	}

#ifndef __DEBUG__
	url = interjecc(url);
#endif

	command += url + " " + target_path;

	execute(command);
}

void cd(const string& path)
{
	cout << "------ CD into " + path << " ------\n";
	throw_unless(chdir(path.c_str()) == 0, runtime_error("Could not change directory to " + path));
}

bool is_cmake(const string& path)
{

	vector<string> file_listing = get_directory_content(path);
	bool is_cmake				= false;

	for (const string& entry : file_listing)
	{

		if (entry == "CMakeLists.txt")
		{
			is_cmake = true;
			break;
		}
	}

	return is_cmake;
}

bool is_qmake(const string& path)
{

	vector<string> file_listing = get_directory_content(path);
	bool is_qmake				= false;

	for (const string& entry : file_listing)
	{

		if (entry.find('.') != string::npos && extract_extention_from_path(entry) == ".pro")
		{
			is_qmake = true;
			break;
		}
	}

	return is_qmake;
}

bool has_header_files(const string& path)
{
	vector<string> file_listing = get_directory_content(path);
	bool header_file_exists		= false;

	for (const string& entry : file_listing)
	{
		if (entry.find('.') != string::npos && extract_extention_from_path(entry) == ".hpp")
		{
			header_file_exists = true;
			break;
		}
	}

	return header_file_exists;
}

bool has_file_with_extension(const string& path, const string& extension)
{
	vector<string> file_listing = get_directory_content(path);
	bool lib_file_exists		= false;

	for (const string& entry : file_listing)
	{
		if (entry.find('.') != string::npos && extract_extention_from_path(entry) == extension)
		{
			lib_file_exists = true;
			break;
		}
	}

	return lib_file_exists;
}

bool has_lib_file(const string& path)
{
	return has_file_with_extension(path, ".a");
}

bool has_so_files(const string& path)
{
	return has_file_with_extension(path, ".so");
}

void qmake()
{
#ifdef __DEBUG__
	string qmake_cmd = R"(qmake "CONFIG += optimize_full" "CONFIG += release")";
#else
	string qmake_cmd = R"(qmake "CONFIG += optimize_full" "CI = enabled" "CONFIG += release")";
#endif

	if (rdi_encrypt)
	{
		qmake_cmd += R"( "DEFINES += RDI_ENCRYPT" "LIBS += -lcryptopp")";
	}

	execute(qmake_cmd);
}

void cmake()
{
#ifdef __DEBUG__
	string qmake_cmd = R"(qmake "CONFIG += optimize_full" "CONFIG += release")";
#else
	string cmake_cmd = R"(cmake -DCMAKE_BUILD_TYPE=Release -DCI=True)";
#endif

	if (rdi_encrypt)
	{
		cmake_cmd += R"( -DRDI_ENCRYPT=True)";
	}

	execute(cmake_cmd);
}

void make()
{
	execute("make -j $(nproc)");
}

void build(string repo_path, const string& subproject)
{
	repo_path += subproject;

	cd(repo_path);
	DEFER(cd(root_repo_path));

	if (subproject.size())
	{
		cout << "=== building subproject: " << subproject << " ===" << endl;
	}

	if (!is_cmake(repo_path))
	{
		if (!is_qmake(repo_path))
			return;

		qmake();
	}
	else
		cmake();
	make();
}

void build(string repo_path = "", vector<string> subprojects = {})
{
	cout << " ----- BUILDING ---- \n"
		 << "--- Root repo = " << root_repo_path << " ------\n";

	if (!repo_path.size())
	{
		repo_path = root_repo_path;
	}

	if (subprojects.empty())
	{
		if (is_directory(repo_path + "lib/"))
		{
			subprojects.push_back("lib/");
		}

		if (is_directory(repo_path + "app/"))
		{
			subprojects.push_back("app/");
		}
	}

	if(subprojects.empty())
	{
		build(repo_path, "");
		return;
	}

	for (const auto& sub : subprojects)
	{
		build(repo_path, sub);
	}
}

void build_test()
{
	string test_path = root_repo_path;

	if (is_directory(root_repo_path + "test/"))
	{
		test_path = test_path + "test/";
	}
	else
	{
		return;
	}

	cd(test_path);
	DEFER(cd(root_repo_path));

	if (!is_cmake(test_path))
	{
		if (!is_qmake(test_path))
			return;

		qmake();
	}
	else
		cmake();

	make();
}

string parse_target_name(const string& path)
{
	for (auto& line : RDI::read_file_lines(path))
	{
		if (line.find("TARGET") != std::string::npos)
		{
			auto tmp = RDI::split(line, '=');

			throw_unless(tmp.size() == 2, "Line is not in the form BLAH = BLAHBLAH");

			string target = tmp[1];

			remove_all_chars(target, ' ');

			return target;
		}
	}

	return "";
}

void install(string repo_path, const string& subproject)
{
	repo_path += subproject;

	cd(repo_path);
	DEFER(cd(root_repo_path));

	if (subproject.size())
	{
		cout << "=== installing subproject: " << subproject << " ===" << endl;
	}

#ifdef __DEBUG__
#define INCLUDE_PREFIX "~/usr/include/"
#define LIB_PREFIX "~/usr/lib/"
#define BIN_PREFIX "~/usr/bin/"
	execute("mkdir -p " INCLUDE_PREFIX " " LIB_PREFIX " " BIN_PREFIX);
#else
#define INCLUDE_PREFIX "/usr/include/"
#define LIB_PREFIX "/usr/lib/"
#define BIN_PREFIX "/usr/bin/"
#endif

	string artifacts_dir = root_repo_path + "artifacts/";
	execute("mkdir -p " + artifacts_dir + "bin/");
	execute("mkdir -p " + artifacts_dir + "lib/");
	execute("mkdir -p " + artifacts_dir + "include/");

	if (file_exists(repo_path + "app.pro"))
	{
		string target = parse_target_name(repo_path + "app.pro");

		if (target.size())
		{
			execute("cp " + target + " " BIN_PREFIX);
			execute("cp " + target + " " + artifacts_dir + "bin/");
		}
	}

	if (has_lib_file(repo_path))
	{
		execute("cp *.a " LIB_PREFIX);
		execute("cp *.a " + artifacts_dir + "lib/");
	}

	if (has_so_files(repo_path))
	{
		execute("cp *.so* " LIB_PREFIX);
		execute("cp *.so* " + artifacts_dir + "lib/");
	}

	if (has_header_files(repo_path))
	{
		execute("cp *.hpp " INCLUDE_PREFIX);
		execute("cp *.hpp " + artifacts_dir + "include/");
	}
}

void install(string repo_path = "", vector<string> subprojects = {})
{
	cout << " ----- INSTALLING ---- \n";

	if (!repo_path.size())
	{
		repo_path = root_repo_path;
	}

	if (subprojects.empty())
	{
		if (is_directory(repo_path + "lib/"))
		{
			subprojects.push_back("lib/");
		}

		if (is_directory(repo_path + "app/"))
		{
			subprojects.push_back("app/");
		}
	}

	if(subprojects.empty())
	{
		install(repo_path, "");
		return;
	}

	for (const auto& sub : subprojects)
	{
		install(repo_path, sub);
	}
}

void install_test()
{
	string test_path = root_repo_path;

	if (is_directory(root_repo_path + "test/"))
	{
		test_path = test_path + "test/";
	}
	else
	{
		return;
	}

	cd(test_path);
	DEFER(cd(root_repo_path));

	if (!is_qmake(test_path) && !is_cmake(test_path))
	{
		return;
	}

	string artifacts_dir = root_repo_path + "artifacts/bin/";
	execute("mkdir -p " + artifacts_dir);

	execute("cp test " + artifacts_dir + "test_bin");
}

bool exists(const string& url)
{
	return dependencies_map.find(url) != dependencies_map.end();
}

/// example input: /tmp/hwfetch/buckwalter/
/// returns commit id
string get_commit_id_of_repo(string repo_path)
{
	repo_path += ".git";

	return get_stdout_from_cmd("git", "--git-dir", repo_path.c_str(), "rev-parse", "HEAD");
}

/// example input: /tmp/hwfetch/buckwalter/
/// returns commit message
string get_commit_message_of_repo(string repo_path)
{
	repo_path += ".git";

	return get_stdout_from_cmd(
		"git", "--git-dir", repo_path.c_str(), "show", "-s", "--format=%B", "HEAD");
}

#ifdef __DEBUG__
#include <regex>

string hack_it(string url)
{
	string sub	 = "https://gitlab.com/rdi-eg/";
	string replace = "git@arwash.rdi:common/";

	regex r(sub);

	string output;

	regex_replace(back_inserter(output), url.begin(), url.end(), r, replace);

	return output;
}

string hack_it_awi(string url)
{
	string sub	 = "http://arwash.rdi:5050/";
	string replace = "git@arwash.rdi:/";

	regex r(sub);

	string output;

	regex_replace(back_inserter(output), url.begin(), url.end(), r, replace);

	return output;
}
#endif

tuple<string, DependencyNode> parse_requires_entry(const string& entry)
{
	vector<string> tokens = split(entry);
	string url;

	DependencyNode node;

	struct ParseState
	{
		bool branch		= false;
		bool subproject = false;
	};

	ParseState parse_state;

	bool first = true;
	for (const auto& t : tokens)
	{
		if (first)
		{
			url = t;
#ifdef __DEBUG__
			url = hack_it(url);
			url = hack_it_awi(url);
#endif
			first = false;
		}
		else if (t == "-b")
		{
			parse_state.branch = true;
		}
		else if (t == "-s")
		{
			parse_state.subproject = true;
		}
		else if (parse_state.branch)
		{
			node.branch = t;
			parse_state = ParseState();
		}
		else if (parse_state.subproject)
		{
			node.subprojects.push_back(t);
			parse_state = ParseState();
		}
		else
		{
			throw runtime_error("bad entry in the requires.txt: " + entry);
		}
	}

	return std::tie(url, node);
}

void insert_dependency_nodes(DependencyNode& root, const vector<string>& requirements)
{
	for (const string& line : requirements)
	{
		if (line.empty())
		{
			continue;
		}

		DependencyNode node;
		string url;
		std::tie(url, node) = parse_requires_entry(line);

		if (!exists(url))
		{
			dependencies_map[url] = node;
		}

		root.dependencies.push_back(url);
	}
}

void resolve_dependencies(const string& url, const string& branch = "")
{
	cout << "resolving " << url << endl;

	DependencyNode& _this = dependencies_map[url];

	if (_this.satisfied)
	{
		cout << "was already resolved, nothing to do :D" << endl;
		return;
	}

	string project_name = extract_projectname(url);
	string repo_path	= REPOS_PATH + project_name + '/';

	clone(url, repo_path, branch);

	cout << repo_path << endl;
	cout << file_exists(repo_path + REQUIRES_FILENAME) << endl;

	throw_unless(file_exists(repo_path + REQUIRES_FILENAME),
				 runtime_error(string("requires.txt is not found for the ") + url + " project :("));

	vector<string> requirements_v = read_file_lines(repo_path + REQUIRES_FILENAME);

	insert_dependency_nodes(_this, requirements_v);

	for (const string& url : _this.dependencies)
	{
		DependencyNode& dep = dependencies_map[url];
		resolve_dependencies(url, dep.branch);
	}

	build(repo_path, _this.subprojects);
	install(repo_path, _this.subprojects);
	_this.satisfied = true;

	string commit_id = get_commit_id_of_repo(repo_path);
	remove_all_chars(commit_id, '\n');

	string commit_message = get_commit_message_of_repo(repo_path);
	remove_all_chars(commit_message, '\n');

	execute("mkdir -p " + root_repo_path + "artifacts/deploy/java/");
	execute("mkdir -p " + root_repo_path + "artifacts/deploy/python/");
	append_to_file(root_repo_path + "artifacts/deploy/commit_ids.txt", url + " " + commit_id);

	cout << "====================resolved======================" << endl;
	cout << "url: " << url << endl;
	cout << "commit_id: " << commit_id << endl;
	cout << "commit_message: " << commit_message << endl;
	cout << "==================================================" << endl;
}

void resolve_dependencies()
{
	cout << "resolving the dependency tree" << endl;
	vector<string> requirements_v = read_file_lines(REQUIRES_FILENAME);

	// root of the dependencies tree
	dependencies_map["root"] = DependencyNode();

	DependencyNode& root = dependencies_map["root"];

	insert_dependency_nodes(root, requirements_v);

	for (const string& url : root.dependencies)
	{
		DependencyNode& dep = dependencies_map[url];
		resolve_dependencies(url, dep.branch);
	}

	build();
	install();
	build_test();
	install_test();
	cout << "all done! :D" << endl;
}

void run_configure(const string& language)
{
	system("chmod +x configure");
	string configure_cmd = "./configure -l " + language + " ";

	if (rdi_encrypt)
	{
		configure_cmd += "-e ";
	}
	if (unit_counter)
	{
		configure_cmd += "-u ";
	}

#ifdef __DEBUG__
	execute(configure_cmd);
#else
	configure_cmd += "-c";
	execute(configure_cmd);
#endif
}

void build_swig(const string& language)
{
	cd("lib/");
	DEFER(cd(root_repo_path));

	if (!file_exists("configure"))
	{
		cout << "no configure file found" << endl;
		return;
	}

	run_configure(language);

	execute("make");

	execute("strip *.so");

	string deploy_dir = root_repo_path + "artifacts/deploy/" + language + "/";
	execute("mkdir -p " + deploy_dir);

	if (language == "python")
	{
		execute("cp *.py " + deploy_dir);
	}
	else if(language == "java")
	{
		execute("cp *.java " + deploy_dir);
	}

	execute("cp *.so " + deploy_dir);
	execute("make clean");
}

void compile_java()
{
	cd("java_test");
	DEFER(cd(root_repo_path));

	string src = get_stdout_from_cmd("find", "src", "-type", "d");

	RDI_Buffer in{ src.data(), src.size() };

	int status;
	RDI_Buffer out = rdi_subprocess_io(&status, in, "tail", "-1", nullptr);
	src			   = string(out.data, out.size);
	rdi_buffer_free(out);

	src.pop_back(); // remove \n
	src += "/";

	cd("..");
	execute("mkdir -p java_test/lib java_test/bin java_test/output");
	string deploy_dir = root_repo_path + "artifacts/deploy/java/";

	execute("cp " + deploy_dir + "*.java java_test/" + src);

	execute("cp " + deploy_dir + "*.so* java_test/lib");

	cd("java_test");

	execute("javac -cp jar/junit-4.12.jar:jar/hamcrest-core-1.3.jar:bin " + src + "/* -d bin");
}

void build_server()
{
	cd("server/");
	DEFER(cd(root_repo_path));

#ifdef __DEBUG__
	execute(R"(qmake "CONFIG += optimize_full" "CONFIG += release")");
#else
	execute(R"(qmake "CONFIG += optimize_full" "CI = enabled" "CONFIG += release")");
#endif

	execute("make -j $(nproc)");

	string artifacts_dir = root_repo_path + "artifacts/";

	string target = parse_target_name(root_repo_path + "/server/server.pro");

	execute("mkdir -p " + artifacts_dir + "server/");
	execute("cp " + target + " " + artifacts_dir + "server/");
}

int __main()
{
	RDI::Timer timer;
	timer.start_timer();

	try
	{
		throw_unless(file_exists(REQUIRES_FILENAME),
					 runtime_error("Couldn't find a requires.txt file."));

		if (file_exists(REPOS_PATH))
		{
			execute("rm -rf " REPOS_PATH);
		}
		cout << "<<<<<<<<<<<<<<<<<< hwfetch version " << HWFETCH_VERSION
			 << " >>>>>>>>>>>>>>>>>>>>>>" << endl;

		execute("mkdir -p " REPOS_PATH);

		resolve_dependencies();

		if (RDI::file_exists("java_test/"))
		{
			build_swig("java");

			compile_java();
		}

		if (RDI::file_exists("python_test/"))
		{
			build_swig("python");
		}

		if (RDI::file_exists("server/"))
		{
			build_server();
		}
	}
	catch (const runtime_error& e)
	{
		cerr << e.what() << endl;

		if (system("rm -rf " REPOS_PATH) != 0)
		{
			cerr << "Could not erase the repos path: " REPOS_PATH "\n";
		}

		return 1;
	}

	cout << "hwfetch elapsed time: " << timer.get_elapsed_time() << "s" << endl;

	return 0;
}

void append_slash(string& str)
{
	if (str.back() == '/')
	{
		return;
	}

	str += '/';
}

int main(int argc, char* argv[])
{
	RDI::Timer timer;
	timer.start_timer();

#ifdef __DEBUG__
	if (argc < 2)
	{
		cerr << "root_repo_path was not provided.\n"
			 << "you should pass it as a command line argument\n"
			 << "Example: hwfetch /home/koko/project/";
		return 1;
	}

	if (argc >= 3 && (string(argv[2]) == "-e" || (argc > 3 && string(argv[3]) == "-e")))
	{
        cout << "encrypt mode is on\n";
		rdi_encrypt = true;
	}

	if(argc >= 3 && (string(argv[2]) == "-u" || (argc > 3 && string(argv[3]) == "-u")))
	{
		cout << "unit counter mode is on\n";
		unit_counter = true;
	}

	root_repo_path = string(argv[1]);

	append_slash(root_repo_path);

	cd(root_repo_path);
#else
    if(argc >1)
    {
        cout << "first command argument is " << argv[1] << endl;
    }
	if (argc >= 2 && (string(argv[1]) == "-e" || (argc > 2 && string(argv[2]) == "-e")))
	{
        cout << "encrypt mode is on\n";
		rdi_encrypt = true;
	}
	if (argc >= 2 && (string(argv[1]) == "-u" || (argc > 2 && string(argv[2]) == "-u")))
	{
		cout << "unit counter mode is on\n";
		unit_counter = true;
	}
#endif

	int ret_status = __main();

	cout << "hwfetch elapsed time: " << timer.get_elapsed_time() << "s" << endl;

	return ret_status;
}
